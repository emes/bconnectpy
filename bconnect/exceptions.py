#-*- coding: utf-8 -*-


class ApiError(Exception):

    def __init__(self, code, description):
        self.code = code
        self.description = description

    def __str__(self):
        return unicode(self).encode('utf-8')

    def __unicode__(self):
        return u'%s: %s' % (self.code, self.description)


class HttpResponseError(ApiError):

    pass


class BankError(ApiError):

    pass


__all__ = [
    'HttpResponseError', 'BankError'
]