from setuptools import setup

setup(
    name='bconnectpy',
    version='1.0.0',
    packages=['bconnect', 'bconnect.tests'],
    url='',
    license='MIT',
    author='ra2er',
    author_email='sylwester.kulpa@gmail.com',
    description='BankConnect API wrapper',
    install_requires=['suds==0.4.1-htj'],
    dependency_links = [
        'git+git://github.com/htj/suds-htj#egg=suds-0.4.1-htj'
    ],
)
